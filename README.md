## Unity Clock Project

Simple project to brush off my Unity skills.

Based initially on the tutorial here: https://catlikecoding.com/unity/tutorials/basics/game-objects-and-scripts/ 


### Available Clocks
1. Tick Tock Clock
2. Analog Clock
3. Digital Clock

![The first three clocks](./Demo.gif)