using System;
using UnityEngine;
using TMPro;

public class DigitalClock : MonoBehaviour {

    [SerializeField]
    TextMeshPro hourText;

    void Update() {
        var time = DateTime.Now;
        hourText.text = time.Hour + ":" + time.Minute;        
    }
}
